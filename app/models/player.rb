class Player < ActiveRecord::Base

  mount_uploader :avatar, AvatarUploader
	has_many :player1_matches, class_name: "Match", foreign_key: "player1_id", dependent: :destroy
	has_many :player2_matches, class_name: "Match", foreign_key: "player2_id", dependent: :destroy
	has_many :points, dependent: :destroy	
  validates_presence_of :f_name, :l_name
  scope :list, -> {order("l_name ASC")}

  # returns first letter from first name and full last name
  def name_with_initial
    "#{f_name.first}. #{l_name}"
  end

  # returns the number of matches
  def all_matches
    player1_matches + player2_matches
  end

  # returns players points
  def sum_value
    sum = 0
    points.each do |v|
      sum += v.value
    end
    sum
  end

  # returns the last players match
  def last_match
    all_matches.last
  end

  # returns the number of wins
  def wins
    counter = 0
    all_matches.each do |m|
      if m.winner_id == id
        counter += 1
      end
    end
    counter
  end

  # returns the number of losses
  def losses
    counter = 0
    all_matches.each do |m|
      if m.loser_id == id
        counter += 1
      end
    end
    counter
  end

  # returns table which contains ids of win matches or lost matches, 
  # depends on passed params, w for wins, l for losses
  def loss_win(res)
    tab = []
    all_matches.each do |m|

      if res == 'w'
        if m.winner_id == id
          tab << m.id
        end
      end

      if res == 'l'
        if m.loser_id == id
          tab << m.id
        end
      end

    end
    tab
  end

end
