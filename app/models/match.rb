class Match < ActiveRecord::Base

	belongs_to :player1, class_name: "Player", foreign_key: "player1_id"
	belongs_to :player2, class_name: "Player", foreign_key: "player2_id"
	has_many :points, dependent: :destroy
	validates_presence_of :player1_id, :player2_id, :date, :player1_res, :player2_res
	validates :player1_res, :player2_res, :numericality => { :greater_than_or_equal_to => 0, :less_than_or_equal_to => 10 }
	validate :the_point, :the_player
	default_scope { order('date DESC') }

	# returns the day of the match
	def day
		date.strftime("%d.%m.%y")
	end

	# returns the match winner id 
	def winner_id
		player1_res > player2_res ? player1_id : player2_id
	end

	# returns the match losser id
	def loser_id
		player1_res < player2_res ? player1_id : player2_id
	end

	# generates Points for match Players
	def generate_points
	  Point.new(:match_id => id, :player_id => player1_id, :value => (10 - player2_res)).save!
      Point.new(:match_id => id, :player_id => player2_id, :value => (10 - player1_res)).save!
	end

	# update points after update the match
	def update_value
	  points[0].update(:player_id => player1_id, :value => (10 - player2_res))
	  points[1].update(:player_id => player2_id, :value => (10 - player1_res))
	end

	# returns matches for selected players in dropdown select
	def self.search(p1,p2)
	  if Match.count != 0
		  if p1.empty? && p2.empty?
		  	all
		  elsif p1.present? && p2.empty?
		  	where(:player1_id => p1)
		  elsif p1.empty? && p2.present?
		  	where(:player2_id => p2)
		  else
		   	where("player1_id LIKE ? AND player2_id LIKE ?", p1, p2)
		  end
	  end
	end

	# validation for results
	def the_point
		if player1_res < 10 && player2_res < 10
			self.errors.add(:player1_res," - one of the results must be 10!")
			self.errors.add(:player2_res," - one of the results must be 10!")
		end
		if player1_res == 10 && player2_res == 10
			self.errors.add(:player1_res, " - just one of the results must be 10!")
			self.errors.add(:player2_res, " - just one of the results must be 10!")
		end
	end

	# validation for player_id
	def the_player
		if player1_id == player2_id
			self.errors.add(:player1_id," - please, change Players")
			self.errors.add(:player2_id, " - please, change Players")
		end
	end
end