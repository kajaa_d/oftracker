class Point < ActiveRecord::Base
  
  belongs_to :match
  belongs_to :player

  validates_presence_of :match_id, :player_id, :value

  # create rank for players
  def self.get_rank(prev_chart = [])
  	chart = Point.find_by_sql("SELECT players.f_name AS f_name, players.l_name AS l_name, players.id AS player_id, sum(value) AS sum 
  							   FROM points 
  							   INNER JOIN players ON points.player_id=players.id 
  							   GROUP BY player_id 
  							   ORDER BY sum(value) desc;")

    new_chart = Hash.new{ |h,k| h[k] = Hash.new(&h.default_proc) }
    # f_name l_name sum prev_position curr_position
    chart.each_with_index do |c,i|
      new_chart[c.player_id][:f_name] = c.f_name
      new_chart[c.player_id][:l_name] = c.l_name
      new_chart[c.player_id][:sum] = c.sum
      new_chart[c.player_id][:curr_position] = i+1
      if !prev_chart.empty? && prev_chart[c.player_id] != nil
          new_chart[c.player_id][:prev_position] = prev_chart[c.player_id][:curr_position]
      else
        new_chart[c.player_id][:prev_position] = -1
      end
    end
    new_chart
  end

end
