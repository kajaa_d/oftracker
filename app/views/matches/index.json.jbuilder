json.array!(@matches) do |match|
  json.extract! match, :id, :date, :player1_id, :player2_id, :player1_res, :player2_res
  json.url match_url(match, format: :json)
end
