json.array!(@players) do |player|
  json.extract! player, :id, :f_name, :l_name, :avatar
  json.url player_url(player, format: :json)
end
