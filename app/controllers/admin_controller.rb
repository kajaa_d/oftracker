class AdminController < ApplicationController
	layout 'user'
	before_action :authenticate_user!

	def rank
		@chart = Chart.first[:chart]
	end
end
