class HomeController < ApplicationController
	layout 'home'

	def index
	  @matches = Match.first(8)
      @players = Player.list
      @chart = Chart.first[:chart]
	end

	def show_player
		logger.info id = params[:id]
		@player = Player.find_by(:id =>id)

		if @player.points.exists?
			chart = Chart.first[:chart]
			@pos = chart[id.to_i][:curr_position]
			@pts = chart[id.to_i][:sum]

			m_wins = @player.loss_win('w')
			@mw_tab = Hash.new
			# opponent points
			m_wins.each_with_index do |mw,i|
				logger.info "================"
				logger.info mw
				match = Match.find_by(:id => mw)
				if match.player1_id == @player.id
					p = Player.find_by(:id => match.player2_id)
					@mw_tab[i] = [p.name_with_initial, match.player2_res]
				else
					p = Player.find_by(:id => match.player1_id)
					@mw_tab[i] = [p.name_with_initial, match.player1_res]
				end
			end

			m_losses = @player.loss_win('l')
			@ml_tab = Hash.new
			# opponent points
			m_losses.each_with_index do |mw,i|
				logger.info "================"
				logger.info mw
				match = Match.find_by(:id => mw)
				if match.player1_id == @player.id
					p = Player.find_by(:id => match.player2_id)
					@ml_tab[i] = [p.name_with_initial, match.player2_res]
				else
					p = Player.find_by(:id => match.player1_id)
					@ml_tab[i] = [p.name_with_initial, match.player1_res]
				end
			end
		end
	end
end
