class MatchesController < ApplicationController
  layout 'user'
  before_action :authenticate_user!
  before_action :set_match, only: [:show, :edit, :update, :destroy]

  # GET /matches
  # GET /matches.json
  def index
    @matches = Match.paginate(:page => params[:page], :per_page => 10)
    @players = Player.all
  end

  # GET /matches/new
  def new
    @match = Match.new
    @players_list = Player.all
  end

  # GET /matches/1/edit
  def edit
    @players_list = Player.all
  end

  # POST /matches
  # POST /matches.json
  def create
    @players_list = Player.all
    @match = Match.new(match_params)

    respond_to do |format|
        if @match.save
          @match.generate_points  
          prev_chart = Chart.first[:chart]    
          Chart.first.update(:chart => Point.get_rank(prev_chart))  
          format.html { redirect_to matches_url, notice: 'Match was successfully created.' }
        else
          format.html { render :new }
          format.json { render json: @match.errors, status: :unprocessable_entity }
        end
    end
  end

  # PATCH/PUT /matches/1
  # PATCH/PUT /matches/1.json
  def update
    @players_list = Player.all
    respond_to do |format|
      if @match.update(match_params) &&  @match.update_value
        prev_chart = Chart.first[:chart]    
        Chart.first.update(:chart => Point.get_rank(prev_chart)) 
        format.html { redirect_to matches_url, notice: 'Match was successfully updated.' }
      else
        format.html { render :edit }
        format.json { render json: @match.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /matches/1
  # DELETE /matches/1.json
  def destroy
    @match.destroy
    respond_to do |format|
      format.html { redirect_to matches_url, notice: 'Match was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def search
    @matches = Match.search(params[:player1_id],params[:player2_id])
      respond_to do |format|
        format.js
      end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_match
      @match = Match.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def match_params
      params.require(:match).permit(:date, :player1_id, :player2_id, :player1_res, :player2_res)
    end
end
