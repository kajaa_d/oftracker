# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Chart.create(chart: Point.get_rank)
User.find_or_create_by!(email: "admin@admin.com") do |admin|
  admin.password = "asdf12345"
  admin.password_confirmation = "asdf12345"
end
