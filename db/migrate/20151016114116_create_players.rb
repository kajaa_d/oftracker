class CreatePlayers < ActiveRecord::Migration
  def change
    create_table :players do |t|
      t.string :f_name
      t.string :l_name
      t.string :avatar

      t.timestamps null: false
    end
  end
end
