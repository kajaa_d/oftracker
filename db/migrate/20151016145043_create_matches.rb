class CreateMatches < ActiveRecord::Migration
  def change
    create_table :matches do |t|
      t.datetime :date
      t.integer :player1_id
      t.integer :player2_id
      t.integer :player1_res
      t.integer :player2_res

      t.timestamps null: false
    end
  end
end
