class CreatePoints < ActiveRecord::Migration
  def change
    create_table :points do |t|
      t.integer :match_id
      t.integer :player_id
      t.integer :value
    end
  end
end
