require 'test_helper'

class PointTest < ActiveSupport::TestCase

  def setup
    @point = points(:fiora_1)
  end

  test "should respond to match" do
    assert_respond_to @point, :match
  end

  test "should respond to player" do
    assert_respond_to @point, :player
  end

  test "invalid wihout attr" do
    @point.match_id = nil
    @point.player_id = nil
    @point.value = nil
    assert_not @point.valid?, "Attr is not being validated"
  end

end
