require 'test_helper'

class PlayerTest < ActiveSupport::TestCase

	def setup
		@player = players(:fiora)
	end

  test "invalid wihout attr" do
  	@player.f_name = nil
  	assert_not @player.valid?, "Attr is not being validated"
  end

  test "valid with all attributes" do
  	assert @player.valid?, "#{@player.errors.full_messages}"
  end

  test "should respond to points" do
  	assert_respond_to @player, :points
  end

  test "should contain points" do
  	assert_equal @player.points, [points(:fiora_1),points(:fiora_2)]
  end

  test "should return name with initial" do 
  	init = "#{@player.f_name[0]}. #{@player.l_name}"
  	assert_equal @player.name_with_initial, init
  end

  test "should return number of players matches" do
  	all_m = @player.player1_matches.count
  	all_m += @player.player2_matches.count
  	assert_equal @player.all_matches.count, all_m
  end

  test "should return points of player" do
  	points = points(:fiora_1).value + points(:fiora_2).value
  	assert_equal @player.sum_value, points
  end  

  test "sholud return last match" do
  	last_m = @player.player1_matches
  	last_m += @player.player2_matches
  	assert_equal @player.last_match, last_m.last
  end

  test "should return number of wins" do
  	assert_equal @player.wins, 1
  end

  test "should return number of losses" do
  	assert_equal @player.losses, 1
  end

  # fiora won 1. match
  # fiora lost 2. match
  test "checks ids of win/loss" do
  	tab_w = [matches(:one).id]
  	tab_l = [matches(:two).id]
  	assert_equal @player.loss_win('w'), tab_w
  	assert_equal @player.loss_win('l'), tab_l
  end

end
