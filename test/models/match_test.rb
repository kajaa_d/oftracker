require 'test_helper'

class MatchTest < ActiveSupport::TestCase

	def setup
		@match = matches(:one)
	end

	def assert_presence(model, field)
		model.valid?
		assert_match /can't be blank/, model. errors[field].join, 
			"Presence error for #{field} not found on #{model.class} "
	end

	test "should return the day of the match"  do
		day = @match.date.strftime("%d.%m.%y")
		assert_equal @match.day, day
	end

	# 1. match won by player1, fiora(id 2)
	test "should return the match winner_id" do
		winner_id = players(:fiora).id
		assert_equal @match.winner_id, winner_id
	end

	# 1. match lost by player2, azir(id 1)
	test "should return the match loser_id" do
		loser_id = players(:azir).id
		assert_equal @match.loser_id, loser_id
	end

	test "should generate points for players" do 
		match = Match.create(date: '2015-10-19 16:50:43', player1_id: 2, 
						  player2_id: 1, player1_res: 6, player2_res: 10)
		match.generate_points
		assert_equal players(:fiora).points.count, 3
	end

	test "should update points" do
		@match.player1_res = 3
		@match.player2_res = 10
		@match.update_value
		assert_equal @match.points[1].value, 7
	end	

	test "should return matches list for selected players" do
		all = Match.search("","")
		assert_equal Match.count, all.count
		p = Match.search("1", "")
		assert_equal 1, p.count
		p1 = Match.search("1", "2")
		assert_equal 1, p1.count
	end

	test "invalid wihout attributions" do
  		@match.player1_id = nil
  		assert_presence(@match, :player1_id)
  		@match.player2_id = nil
  		assert_presence(@match, :player2_id)
  		@match.date = nil
  		assert_presence(@match, :date)
  	end

  	test "valid results" do
  		@match.player1_res = 11
  		assert !@match.valid?
  	end

  	test "valid players" do 
  		@match.player1_id = 2
  		@match.player2_id = 2
  		@match.valid?
  		assert_match /- please, change Players/, @match. errors[:player1_id].join, 
			"Diffrent IDs for Players"
  	end
end
