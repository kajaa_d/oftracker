-- MySQL dump 10.13  Distrib 5.5.44, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: oftracker
-- ------------------------------------------------------
-- Server version	5.5.44-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `charts`
--

DROP TABLE IF EXISTS `charts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `charts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chart` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `charts`
--

LOCK TABLES `charts` WRITE;
/*!40000 ALTER TABLE `charts` DISABLE KEYS */;
INSERT INTO `charts` VALUES (6,'---\n24:\n  :f_name: \'Vi \'\n  :l_name: Piltover\n  :sum: 26\n  :curr_position: 1\n  :prev_position: 1\n17:\n  :f_name: Dr\n  :l_name: Mundo\n  :sum: 25\n  :curr_position: 2\n  :prev_position: 2\n22:\n  :f_name: Sona\n  :l_name: Arcade\n  :sum: 17\n  :curr_position: 3\n  :prev_position: 3\n19:\n  :f_name: Lulu\n  :l_name: LuLu\n  :sum: 11\n  :curr_position: 4\n  :prev_position: 4\n23:\n  :f_name: Tee\n  :l_name: Mo\n  :sum: 10\n  :curr_position: 5\n  :prev_position: 5\n25:\n  :f_name: Zilean\n  :l_name: Chronokeeper\n  :sum: 10\n  :curr_position: 6\n  :prev_position: 10\n20:\n  :f_name: Lux\n  :l_name: Luxxx\n  :sum: 8\n  :curr_position: 7\n  :prev_position: 6\n18:\n  :f_name: Fizz\n  :l_name: Fish\n  :sum: 7\n  :curr_position: 8\n  :prev_position: 7\n21:\n  :f_name: Miss\n  :l_name: Fortune\n  :sum: 6\n  :curr_position: 9\n  :prev_position: 8\n16:\n  :f_name: Blitz\n  :l_name: Crank\n  :sum: 2\n  :curr_position: 10\n  :prev_position: 9\n'),(7,'---\n24:\n  :f_name: \'Vi \'\n  :l_name: Piltover\n  :sum: 26\n  :curr_position: 1\n  :prev_position: -1\n17:\n  :f_name: Dr\n  :l_name: Mundo\n  :sum: 25\n  :curr_position: 2\n  :prev_position: -1\n22:\n  :f_name: Sona\n  :l_name: Arcade\n  :sum: 17\n  :curr_position: 3\n  :prev_position: -1\n19:\n  :f_name: Lulu\n  :l_name: LuLu\n  :sum: 11\n  :curr_position: 4\n  :prev_position: -1\n23:\n  :f_name: Tee\n  :l_name: Mo\n  :sum: 10\n  :curr_position: 5\n  :prev_position: -1\n25:\n  :f_name: Zilean\n  :l_name: Chronokeeper\n  :sum: 10\n  :curr_position: 6\n  :prev_position: -1\n20:\n  :f_name: Lux\n  :l_name: Luxxx\n  :sum: 8\n  :curr_position: 7\n  :prev_position: -1\n18:\n  :f_name: Fizz\n  :l_name: Fish\n  :sum: 7\n  :curr_position: 8\n  :prev_position: -1\n21:\n  :f_name: Miss\n  :l_name: Fortune\n  :sum: 6\n  :curr_position: 9\n  :prev_position: -1\n16:\n  :f_name: Blitz\n  :l_name: Crank\n  :sum: 2\n  :curr_position: 10\n  :prev_position: -1\n');
/*!40000 ALTER TABLE `charts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `matches`
--

DROP TABLE IF EXISTS `matches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `matches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `player1_id` int(11) DEFAULT NULL,
  `player2_id` int(11) DEFAULT NULL,
  `player1_res` int(11) DEFAULT NULL,
  `player2_res` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `matches`
--

LOCK TABLES `matches` WRITE;
/*!40000 ALTER TABLE `matches` DISABLE KEYS */;
INSERT INTO `matches` VALUES (36,'2015-10-12 13:01:00',16,17,4,10,'2015-10-20 13:02:16','2015-10-20 13:02:16'),(42,'2015-10-12 13:05:00',17,18,10,4,'2015-10-20 13:06:02','2015-10-20 13:06:02'),(43,'2015-10-20 13:06:00',19,20,3,10,'2015-10-20 13:06:18','2015-10-20 13:06:18'),(44,'2015-10-12 13:06:00',20,21,4,10,'2015-10-20 13:06:31','2015-10-20 13:06:31'),(45,'2015-10-20 13:06:00',22,21,10,7,'2015-10-20 13:06:41','2015-10-20 13:06:41'),(46,'2015-10-15 13:06:00',24,23,5,10,'2015-10-20 13:06:56','2015-10-20 13:06:56'),(47,'2015-10-20 13:06:00',17,16,10,5,'2015-10-20 13:07:07','2015-10-20 13:07:07'),(48,'2015-10-20 13:07:00',18,24,2,10,'2015-10-20 13:07:18','2015-10-20 13:07:18'),(49,'2015-10-20 13:07:00',25,24,1,10,'2015-10-20 13:07:27','2015-10-20 13:07:27'),(50,'2015-10-17 13:07:00',19,18,3,10,'2015-10-20 13:07:40','2015-10-20 13:07:40'),(51,'2015-10-16 13:07:00',22,24,10,2,'2015-10-20 13:07:56','2015-10-20 13:07:56'),(52,'2015-10-20 13:07:00',20,16,8,10,'2015-10-20 13:08:08','2015-10-20 13:08:08'),(53,'2015-10-20 13:08:00',17,25,10,9,'2015-10-20 13:08:19','2015-10-20 13:08:19'),(54,'2015-10-20 13:08:00',24,17,3,10,'2015-10-20 13:08:26','2015-10-20 13:08:26'),(55,'2015-10-20 13:08:00',18,23,5,10,'2015-10-20 13:08:36','2015-10-20 13:08:36'),(56,'2015-10-20 13:08:00',19,22,4,10,'2015-10-20 13:08:45','2015-10-20 13:08:45'),(57,'2015-10-20 13:08:00',20,21,10,9,'2015-10-20 13:08:55','2015-10-20 13:08:55'),(58,'2015-10-20 13:09:00',19,16,10,7,'2015-10-20 13:09:20','2015-10-20 13:09:20'),(59,'2015-10-15 13:09:00',21,19,2,10,'2015-10-20 13:09:32','2015-10-20 13:09:32'),(60,'2015-10-20 18:03:00',24,22,10,1,'2015-10-20 18:04:35','2015-10-20 18:04:35'),(61,'2015-10-21 08:44:00',20,25,0,10,'2015-10-21 08:44:41','2015-10-21 08:45:00');
/*!40000 ALTER TABLE `matches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `players`
--

DROP TABLE IF EXISTS `players`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `players` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `f_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `l_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `players`
--

LOCK TABLES `players` WRITE;
/*!40000 ALTER TABLE `players` DISABLE KEYS */;
INSERT INTO `players` VALUES (16,'Blitz','Crank','Blitzcrank.png','2015-10-20 12:54:23','2015-10-20 12:54:23'),(17,'Dr','Mundo','DrMundo.png','2015-10-20 12:54:33','2015-10-20 12:54:33'),(18,'Fizz','Fish','Fizz.png','2015-10-20 12:54:49','2015-10-20 12:55:31'),(19,'Lulu','LuLu','Lulu.png','2015-10-20 12:55:04','2015-10-20 12:55:04'),(20,'Lux','Luxxx','Lux.png','2015-10-20 12:55:20','2015-10-20 12:55:20'),(21,'Miss','Fortune','MissFortune.png','2015-10-20 12:55:43','2015-10-20 12:55:43'),(22,'Sona','Arcade','Sona.png','2015-10-20 12:55:55','2015-10-20 12:55:55'),(23,'Tee','Mo','Teemo.png','2015-10-20 12:56:06','2015-10-20 12:56:06'),(24,'Vi ','Piltover','Vi.png','2015-10-20 12:56:36','2015-10-20 12:56:36'),(25,'Zilean','Chronokeeper','Zilean.png','2015-10-20 12:57:01','2015-10-20 12:57:01'),(26,'Cho','Ghat',NULL,'2015-10-21 10:26:38','2015-10-21 10:26:38');
/*!40000 ALTER TABLE `players` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `points`
--

DROP TABLE IF EXISTS `points`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `points` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `match_id` int(11) DEFAULT NULL,
  `player_id` int(11) DEFAULT NULL,
  `value` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `points`
--

LOCK TABLES `points` WRITE;
/*!40000 ALTER TABLE `points` DISABLE KEYS */;
INSERT INTO `points` VALUES (39,36,16,0),(40,36,17,6),(51,42,17,6),(52,42,18,0),(53,43,19,0),(54,43,20,7),(55,44,20,0),(56,44,21,6),(57,45,22,3),(58,45,21,0),(59,46,24,0),(60,46,23,5),(61,47,17,5),(62,47,16,0),(63,48,18,0),(64,48,24,8),(65,49,25,0),(66,49,24,9),(67,50,19,0),(68,50,18,7),(69,51,22,8),(70,51,24,0),(71,52,20,0),(72,52,16,2),(73,53,17,1),(74,53,25,0),(75,54,24,0),(76,54,17,7),(77,55,18,0),(78,55,23,5),(79,56,19,0),(80,56,22,6),(81,57,20,1),(82,57,21,0),(83,58,19,3),(84,58,16,0),(85,59,21,0),(86,59,19,8),(87,60,24,9),(88,60,22,0),(89,61,20,0),(90,61,25,10);
/*!40000 ALTER TABLE `points` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schema_migrations`
--

DROP TABLE IF EXISTS `schema_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schema_migrations` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `unique_schema_migrations` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schema_migrations`
--

LOCK TABLES `schema_migrations` WRITE;
/*!40000 ALTER TABLE `schema_migrations` DISABLE KEYS */;
INSERT INTO `schema_migrations` VALUES ('20151016114116'),('20151016115520'),('20151016145043'),('20151018124714'),('20151020074839'),('20151021162914');
/*!40000 ALTER TABLE `schema_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `encrypted_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `reset_password_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_password_sent_at` datetime DEFAULT NULL,
  `remember_created_at` datetime DEFAULT NULL,
  `sign_in_count` int(11) NOT NULL DEFAULT '0',
  `current_sign_in_at` datetime DEFAULT NULL,
  `last_sign_in_at` datetime DEFAULT NULL,
  `current_sign_in_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_sign_in_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_users_on_email` (`email`),
  UNIQUE KEY `index_users_on_reset_password_token` (`reset_password_token`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin@admin.com','$2a$10$mzi1hRvdZ7XNtRJ2umIB5um6d3qR269MaFYzy0iDSy64aznJW6SXO',NULL,NULL,NULL,7,'2015-10-21 16:56:46','2015-10-21 16:56:39','::1','::1','2015-10-21 16:35:16','2015-10-21 16:56:46');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-10-21 19:00:24
